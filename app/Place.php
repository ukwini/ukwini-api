<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model {

	protected $hidden = array('created_at',
							  'updated_at',
							  'latitude',
							  'longitude');
	/**
	* Um Place pode ter varios places
	* "Pode mandar chave para varias tabelas"	
	*/
	public function places()
	{
		return $this->hasMany('App\Place');
	}

	/**
	* Este Place pertence a algum place Por isso,
	* receber uma chave!
	*/
	public function place()
	{
		return $this->belongsTo('App\Place');
	}
}