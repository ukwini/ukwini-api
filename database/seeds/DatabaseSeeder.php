<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Place;
use App\User;
use App\Credential;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//Model::unguard();

		// $this->call('UserTableSeeder');
		DB::table('places')->delete();
		DB::table('credentials')->delete();
		DB::table('users')->delete();

		/**
		*	Criar usuario
		*/
		$user1 = new User;
		$user1->name = 'Elton Laice';
		$user1->password = Hash::make(1357911);
		$user1->email = 'eltonlaice@sapo.mz';
		$user1->save();

		$user2 = new User;
		$user2->name = 'Tedy Macie';
		$user2->password = Hash::make(24681012);
		$user2->email = 'eltonlaice@hotmail.mz';
		$user2->save();
		/**
		*	------Fim Criar usuario
		*/


		/**
		* Criar Credentials para usuarios
		*/	
		$credential1 = new Credential;
		$credential1->key = sha1(str_random(6));
		$credential1->isActive = false;
		$credential1->user()->associate($user1);
		$credential1->save();

		$credential2 = new Credential;
		$credential2->key = sha1(str_random(6));
		$credential2->isActive = false;
		$credential2->user()->associate($user2);
		$credential2->save();
		/**
		* -------Fim Criar Credentials para usuarios
		*/		



		$cont = new Place;
		$cont->name = 'África';
		$cont->latitude = null;
		$cont->longitude = null;
		$cont->level = 1;
		$cont->place_id = 1;
		$cont->save();

		$country = new Place;
		$country->name = 'Moçambique';
		$country->latitude = null;
		$country->longitude = null;
		$country->level = 2;
		$country->place_id = $cont->id;
		$country->save();

		$province1 = new Place;
		$province1->name = 'Maputo Cidade';
		$province1->latitude = null;
		$province1->longitude = null;
		$province1->level = 3;
		$province1->place_id = $country->id;
		$province1->save();

		$province2 = new Place;
		$province2->name = 'Maputo Província';
		$province2->latitude = null;
		$province2->longitude = null;
		$province2->level = 3;
		$province2->place_id = $country->id;
		$province2->save();		

		$province3 = new Place;
		$province3->name = 'Gaza';
		$province3->latitude = null;
		$province3->longitude = null;
		$province3->level = 3;
		$province3->place_id = $country->id;
		$province3->save();

		$province4 = new Place;
		$province4->name = 'Inhambane';
		$province4->latitude = null;
		$province4->longitude = null;
		$province4->level = 3;
		$province4->place_id = $country->id;
		$province4->save();

		$province5 = new Place;
		$province5->name = 'Manica';
		$province5->latitude = null;
		$province5->longitude = null;
		$province5->level = 3;
		$province5->place_id = $country->id;
		$province5->save();

		$province6 = new Place;
		$province6->name = 'Sofala';
		$province6->latitude = null;
		$province6->longitude = null;
		$province6->level = 3;
		$province6->place_id = $country->id;
		$province6->save();

		$province7 = new Place;
		$province7->name = 'Zambézia';
		$province7->latitude = null;
		$province7->longitude = null;
		$province7->level = 3;
		$province7->place_id = $country->id;
		$province7->save();

		$province8 = new Place;
		$province8->name = 'Tete';
		$province8->latitude = null;
		$province8->longitude = null;
		$province8->level = 3;
		$province8->place_id = $country->id;
		$province8->save();

		$province9 = new Place;
		$province9->name = 'Nampula';
		$province9->latitude = null;
		$province9->longitude = null;
		$province9->level = 3;
		$province9->place_id = $country->id;
		$province9->save();

		$province10 = new Place;
		$province10->name = 'Niassa';
		$province10->latitude = null;
		$province10->longitude = null;
		$province10->level = 3;
		$province10->place_id = $country->id;
		$province10->save();

		$province11 = new Place;
		$province11->name = 'Cabo delgado';
		$province11->latitude = null;
		$province11->longitude = null;
		$province11->level = 3;
		$province11->place_id = $country->id;
		$province11->save();




		/**
		*	Distritos de Cabo delgado
		*/
		$district1 = new Place;
		$district1->name = 'Ancuabe';
		$district1->level = 4;
		$district1->place_id = $province11->id;
		$district1->save();
		

		$district2 = new Place;
		$district2->name = 'Balama';
		$district2->level = 4;
		$district2->place_id = $province11->id;
		$district2->save();

		$district3 = new Place;
		$district3->name = 'Chiúre';
		$district3->level = 4;
		$district3->place_id = $province11->id;
		$district3->save();

		$district4 = new Place;
		$district4->name = 'Ibo';
		$district4->level = 4;
		$district4->place_id = $province11->id;
		$district4->save();

		$district5 = new Place;
		$district5->name = 'Macomia';
		$district5->level = 4;
		$district5->place_id = $province11->id;
		$district5->save();

		$district6 = new Place;
		$district6->name = 'Mecúfi';
		$district6->level = 4;
		$district6->place_id = $province11->id;
		$district6->save();

		$district7 = new Place;
		$district7->name = 'Meluco';
		$district7->level = 4;
		$district7->place_id = $province11->id;
		$district7->save();

		$district8 = new Place;
		$district8->name = 'Metuge';
		$district8->level = 4;
		$district8->place_id = $province11->id;
		$district8->save();

		$district9 = new Place;
		$district9->name = 'Mocímboa da Praia';
		$district9->level = 4;
		$district9->place_id = $province11->id;
		$district9->save();

		$district10 = new Place;
		$district10->name = 'Montepuez';
		$district10->level = 4;
		$district10->place_id = $province11->id;
		$district10->save();




		/**
		*	Distritos de Niassa
		*/
		$district11 = new Place;
		$district11->name = 'Chimbonila';
		$district11->level = 4;
		$district11->place_id = $province10->id;
		$district11->save();
		

		$district12 = new Place;
		$district12->name = 'Cuamba';
		$district12->level = 4;
		$district12->place_id = $province10->id;
		$district12->save();

		$district13 = new Place;
		$district13->name = 'Lago';
		$district13->level = 4;
		$district13->place_id = $province10->id;
		$district13->save();

		$district14 = new Place;
		$district14->name = 'Lichinga';
		$district14->level = 4;
		$district14->place_id = $province10->id;
		$district14->save();


		/**
		*	Distritos de Tete
		*/
		$district15 = new Place;
		$district15->name = 'Angónia';
		$district15->level = 4;
		$district15->place_id = $province8->id;
		$district15->save();
		

		$district16 = new Place;
		$district16->name = 'Cahora-Bassa';
		$district16->level = 4;
		$district16->place_id = $province8->id;
		$district16->save();

		$district17 = new Place;
		$district17->name = 'Changara';
		$district17->level = 4;
		$district17->place_id = $province8->id;
		$district17->save();

		$district18 = new Place;
		$district18->name = 'Tsangano';
		$district18->level = 4;
		$district18->place_id = $province8->id;
		$district18->save();

		/**
		*	Distritos de Zambezia
		*/
		$district19 = new Place;
		$district19->name = 'Alto Molócuè';
		$district19->level = 4;
		$district19->place_id = $province7->id;
		$district19->save();
		

		$district20 = new Place;
		$district20->name = 'Chinde';
		$district20->level = 4;
		$district20->place_id = $province7->id;
		$district20->save();

		$district20 = new Place;
		$district20->name = 'Maganja da Costa';
		$district20->level = 4;
		$district20->place_id = $province7->id;
		$district20->save();

		$district21 = new Place;
		$district21->name = 'Namarroi';
		$district21->level = 4;
		$district21->place_id = $province7->id;
		$district21->save();


		/**
		*	Distritos de Inhambane
		*/
		$district22 = new Place;
		$district22->name = 'Funhalouro';
		$district22->level = 4;
		$district22->place_id = $province4->id;
		$district22->save();
		

		$district23 = new Place;
		$district23->name = 'Govuro';
		$district23->level = 4;
		$district23->place_id = $province4->id;
		$district23->save();

		$district24 = new Place;
		$district24->name = 'Inhambane';
		$district24->level = 4;
		$district24->place_id = $province4->id;
		$district24->save();

		$district25 = new Place;
		$district25->name = 'Zavala';
		$district25->level = 4;
		$district25->place_id = $province4->id;
		$district25->save();

		/**
		*	Distritos de Maputo Provincia
		*/
		$district26 = new Place;
		$district26->name = 'Boane';
		$district26->level = 4;
		$district26->place_id = $province2->id;
		$district26->save();
		

		$district27 = new Place;
		$district27->name = 'Magude';
		$district27->level = 4;
		$district27->place_id = $province2->id;
		$district27->save();

		$district28 = new Place;
		$district28->name = 'Manhiça';
		$district28->level = 4;
		$district28->place_id = $province2->id;
		$district28->save();

		$district29 = new Place;
		$district29->name = 'Matola';
		$district29->level = 4;
		$district29->place_id = $province2->id;
		$district29->save();

		$district30 = new Place;
		$district30->name = 'Marracuene';
		$district30->level = 4;
		$district30->place_id = $province2->id;
		$district30->save();

		$district31 = new Place;
		$district31->name = 'Matutuíne';
		$district31->level = 4;
		$district31->place_id = $province2->id;
		$district31->save();

		$district32 = new Place;
		$district32->name = 'Moamba';
		$district32->level = 4;
		$district32->place_id = $province2->id;
		$district32->save();

		$district33 = new Place;
		$district33->name = 'Namaacha';
		$district33->level = 4;
		$district33->place_id = $province2->id;
		$district33->save();

		//Postos Administrativos do distrito de Manhica
		$post34 = new Place;
		$post34->name = 'Calanga';
		$post34->level = 5;
		$post34->place_id = $district28->id;
		$post34->save();

		$post34 = new Place;
		$post34->name = 'Ilha Josina Machel:';
		$post34->level = 5;
		$post34->place_id = $district28->id;
		$post34->save();

		$post34 = new Place;
		$post34->name = 'Maluana';
		$post34->level = 5;
		$post34->place_id = $district28->id;
		$post34->save();


		//Postos Administrativos do distrito de Balama(Cabo delgado)
		$post35 = new Place;
		$post35->name = ' Balama';
		$post35->level = 5;
		$post35->place_id = $district2->id;
		$post35->save();

		$post36 = new Place;
		$post36->name = 'Impiiri';
		$post36->level = 5;
		$post36->place_id = $district2->id;
		$post36->save();

		$post37 = new Place;
		$post37->name = 'Kuékué';
		$post37->level = 5;
		$post37->place_id = $district2->id;
		$post37->save();

		$post37 = new Place;
		$post37->name = 'Mavala';
		$post37->level = 5;
		$post37->place_id = $district2->id;
		$post37->save();


		//Postos Administrativos do distrito de Chinde
		$post38 = new Place;
		$post38->name = 'Chinde - Sede';
		$post38->level = 5;
		$post38->place_id = $district20->id;
		$post38->save();

		$post39 = new Place;
		$post39->name = 'Micaune';
		$post39->level = 5;
		$post39->place_id = $district20->id;
		$post39->save();

		$post40 = new Place;
		$post40->name = 'Maluana';
		$post40->level = 5;
		$post40->place_id = $district28->id;
		$post40->save();
	}
}