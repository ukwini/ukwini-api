<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ProvinceDistrictPostController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($provinceId, $districtId, Request $request)
	{
		$name  	  = $request->get('name');
		$key  	  = $request->get('key');

		$level = Service::LEVEL_POST;

		if(strlen($name) > 0){
			return Service::placeAllByIdIdName($provinceId, $districtId, $name, $level, $key);
		}else{
			return Service::placeAllByIdId($provinceId, $districtId, $level, $key);	
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($provinceId, $districtId, $postId, Request $request)
	{
		$key  	  = $request->get('key');
		$level = Service::LEVEL_POST;

		return Service::placeByIdIdId($provinceId, $districtId, $postId, $level, $key);
	}
}