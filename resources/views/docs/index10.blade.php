@extends('app')

@section('header-titulo')
	Documentação | {infoice}
@endsection


@section('content')

	<div class="row">
		<div class="col-md-12 titulo">
			<h2>API da Divisão administrativa de Moçambique</h2>
			<h3>Documentação</h3>
			<hr>
		</div>
	</div>

	<div class="row principal">

	</div>	
	<div class="row">
		<div class="col-md-3">
			<ul>
				<li><h4><a href="" title="">Introdução</a></h4></li>
				<li><h4><a href="" title="">Hierarquias</a></h4></li>
				<li><h4><a href="" title="">API Key</a></h4></li>

				<li><h4><a href="#requisicoes" title="">Requisições</a></h4>
					<ul>
						<li><a href="#provinces" title="">Províncias</a></li>
						<li><a href="#districts" title="">Distritos</a></li>
						<li><a href="#posts" title="">Postos Administrativos</a></li>
						<li><a href="#places" title="">Places</a></li>
					</ul>
				</li>

				<li><h4><a href="#error" title="">Error</a></h4>
					<ul>
						<li><a href="#error" title="">200 status</a></li>
						<li><a href="#error" title="">404 status</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="col-md-9">
			<hr>
				<h3>Introdução</h3>
				<p class="texto">
				Bem vindo a infoice, API da divisão administrativa de moçambique. Sistema que disponibiliza informação organizada e categorizada de Províncias, distritos, Postos Administrativos, Localidades, Vilas, Bairros, Cidades e Municípios através de requisições HTTP. <br><br>E todos os resultados são apresentados no formato <code>json</code>. </p>
				
				<h3>Hierarquias</h3>
				<p class="texto">Os Lugares geográficos que compõem o pais são nessa especificação, denominados <code>places</code>, que por sua vez categorizados em <code>level</code>. 
				<br> Level define a hierarquia dos places. Veja a tabela abaixo:  </p>
				<br>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Categorias</th>
							<th>Level</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>País</td>
							<td>2</td>
						</tr>

						<tr>
							<td>Província</td>
							<td>3</td>
						</tr>

						<tr>
							<td>Distrito</td>
							<td>4</td>
						</tr>

						<tr>
							<td>Posto Administrativo</td>
							<td>5</td>
						</tr>

						<tr>
							<td>Localidade</td>
							<td>6</td>
						</tr>

						<tr>
							<td>Cidade</td>
							<td>7</td>
						</tr>
					</tbody>
				</table>


				<h3>API Key</h3>
				<p class="texto">Para usar a API infoice, isto é, para fazer requisições de qualquer informação é necessário a apresentação de credenciais. Neste caso, existe a <code>key</code>, que é um atributo obrigatório em todas requisições. <br>
				<br>Exemplo de uma key:</p>
				<img src="{{asset('/image/keys.png')}}" alt="">				
				<br><br>
				<p class="texto">Para obter uma key <a href="/auth/register" title="">clique aqui</a> e siga as seguintes instruções:</p>
					<ol>
						<li><p><a href="/auth/register" title="">Cadastrar-se</a> no Sistema.</p></li>
						<li><p>Criar uma app, que ira usar a key.</p></li>
						<li><p>E usar. </p></li>
					</ol>


				<br>
				<h3 id="requisicoes">Requisições</h3>
								<p>Todas requisições de <code>place</code> terá com resposta um json como no exemplo em abaixo:  </p>

				<pre>
{
    "places": [
        {
            "id": 14,
            "name": "Ancuabe",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 13,
                "name": "Cabo delgado",
                "level": 3,
                "description": "Província"
            }
        },
        {
            "id": 15,
            "name": "Balama",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 13,
                "name": "Cabo delgado",
                "level": 3,
                "description": "Província"
            }
        }
    ]
}	
				</pre>

				<p>Lengenda dos campos</p>

				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Campo</th>
							<th>Descrição</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>id</td>
							<td>Identificador único para place</td>
						</tr>
						<tr>
							<td>name</td>
							<td>Nome de places </td>
						</tr>
						<tr>
							<td>level</td>
							<td>Numero que representa a categoria do place</td>
						</tr>
						<tr>
							<td>description</td>
							<td>Nome da categoria do place</td>
						</tr>
						<tr>
							<td>in_place</td>
							<td>indica o place de nivel superior onde esta situada.</td>
						</tr>
					</tbody>
				</table>



				
				<br>
				<br>
				<h3 id="provinces"><b>/provinces</b></h3>
					<p><b>URL complecta: </b><code>http://infoice.herokuapp.com/api/v1.0/provinces?key=YOUR_APP_KEY</code></p>
				<p><b>Descrição: </b>Retorna todas províncias do País.</p>



				<br>
				<br>
				<h3><b>/provinces/{province_id}</b></h3>
					<p><b>URL complecta: </b><code>http://infoice.herokuapp.com/api/v1.0/provinces/{province_id}?key=YOUR_APP_KEY</code></p>
				<p><b>Descrição: </b>Retorna uma província do País por ID</p>

				<p><b>Exemplo: </b><code>http://infoice.herokuapp.com/api/v1.0/provinces/8?key=YOUR_APP_KEY</code></p>
<pre>
{
    "place": {
        "id": 8,
        "name": "Sofala",
        "level": 3,
        "description": "Província",
        "in_place": {
            "id": 2,
            "name": "Moçambique",
            "level": 2,
            "description": "País"
        }
    }
}
</pre>
				<br>
				<br>
				<h3><b>/provinces?parameters</b></h3>
					<p><b>URL complecta: </b><code>http://infoice.herokuapp.com/api/v1.0/provinces?parameters&key=YOUR_APP_KEY</code></p>
				<p><b>Descrição: </b>Retorna todas províncias do País que de acordo com os parâmetros estabelecidos.</p>

					<p><b>Parâmetros: </b></p>
					<ul>
						<li><p><code>name:</code>Variável permite definir nome de places que seram pesquisados. Por exemplo, se definir-se <code>name=maput</code> seram retornados places que tenham um name similar.</p></li>
					</ul>
					
					<br>
					<p><b>Exemplo: </b><code>http://infoice.herokuapp.com/api/v1.0/provinces?name=Maputo&key=YOUR_APP_KEY</code></p>

<pre>
{
    "places": [
        {
            "id": 3,
            "name": "Maputo Cidade",
            "level": 3,
            "description": "Província",
            "in_place": {
                "id": 2,
                "name": "Moçambique",
                "level": 2,
                "description": "País"
            }
        },
        {
            "id": 4,
            "name": "Maputo Província",
            "level": 3,
            "description": "Província",
            "in_place": {
                "id": 2,
                "name": "Moçambique",
                "level": 2,
                "description": "País"
            }
        }
    ]
}
</pre>


				<br>
				<h3 id="districts"><b>/provinces/{province_id}/districts</b></h3>
				<p><b>URL complecta: </b><code>http://infoice.herokuapp.com/api/v1.0/provinces/{province_id}/districts?key=YOUR_APP_KEY</code></p>
				<p><b>Descrição: </b>Retorna todos distritos que pertencem à uma província definida por <code>province_id</code> </p>

				<p><b>Exemplo: </b><code>http://infoice.herokuapp.com/api/v1.0/provinces/4/districts?key=YOUR_APP_KEY</code></p>
				<div class="resposta">
<pre>
{
    "places": [
        {
            "id": 40,
            "name": "Boane",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 4,
                "name": "Maputo Província",
                "level": 3,
                "description": "Província"
            }
        },
        {
            "id": 41,
            "name": "Magude",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 4,
                "name": "Maputo Província",
                "level": 3,
                "description": "Província"
            }
        },
        {
            "id": 42,
            "name": "Manhiça",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 4,
                "name": "Maputo Província",
                "level": 3,
                "description": "Província"
            }
        },
        {
            "id": 43,
            "name": "Matola",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 4,
                "name": "Maputo Província",
                "level": 3,
                "description": "Província"
            }
        },
        {
            "id": 44,
            "name": "Marracuene",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 4,
                "name": "Maputo Província",
                "level": 3,
                "description": "Província"
            }
        },
        {
            "id": 45,
            "name": "Matutuíne",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 4,
                "name": "Maputo Província",
                "level": 3,
                "description": "Província"
            }
        },
        {
            "id": 46,
            "name": "Moamba",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 4,
                "name": "Maputo Província",
                "level": 3,
                "description": "Província"
            }
        },
        {
            "id": 47,
            "name": "Namaacha",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 4,
                "name": "Maputo Província",
                "level": 3,
                "description": "Província"
            }
        }
    ]
}
</pre> 
</div>

				
				<br>
				<h3><b>/provinces/{province_id}/districts/{district_id}</b></h3>
				
				
				<p><b>URL complecta: </b><code>http://infoice.herokuapp.com/api/v1.0/provinces/{province_id}/districts/{district_id}?key=YOUR_APP_KEY</code></p>
				<p><b>Descrição: </b>Retorna um distrito por ID, que pertencem à uma província definida por <code>province_id</code> </p>

				<p><b>Exemplo: </b><code>http://infoice.herokuapp.com/api/v1.0/provinces/4/districts/43?key=YOUR_APP_KEY</code></p>
<pre>
{
    "place": {
        "id": 43,
        "name": "Matola",
        "level": 4,
        "description": "Distrito",
        "in_place": {
            "id": 4,
            "name": "Maputo Província",
            "level": 3,
            "description": "Província"
        }
    }
}
</pre>

				<br>
				<h3 id="posts"><b>/provinces/{province_id}/districts/{district_id}/posts</b></h3>
				<p><b>URL complecta: </b><code>http://infoice.herokuapp.com/api/v1.0/provinces/{province_id}/districts/{district_id}/posts?key=YOUR_APP_KEY</code></p>
				<p><b>Descrição: </b>Retorna todos os Postos Administrativos que pertencem à um Districto definido por <code>district_id</code>.</p>

				<p><b>Exemplo: </b><code>http://infoice.herokuapp.com/api/v1.0/provinces/4/districts/42/posts?key=YOUR_APP_KEY</code></p>
				
<pre>
{
    "places": [
        {
            "id": 48,
            "name": "Calanga",
            "level": 5,
            "description": "Posto Administrativo",
            "in_place": {
                "id": 42,
                "name": "Manhiça",
                "level": 4,
                "description": "Distrito"
            }
        },
        {
            "id": 49,
            "name": "Ilha Josina Machel:",
            "level": 5,
            "description": "Posto Administrativo",
            "in_place": {
                "id": 42,
                "name": "Manhiça",
                "level": 4,
                "description": "Distrito"
            }
        },
        {
            "id": 50,
            "name": "Maluana",
            "level": 5,
            "description": "Posto Administrativo",
            "in_place": {
                "id": 42,
                "name": "Manhiça",
                "level": 4,
                "description": "Distrito"
            }
        },
        {
            "id": 57,
            "name": "Maluana",
            "level": 5,
            "description": "Posto Administrativo",
            "in_place": {
                "id": 42,
                "name": "Manhiça",
                "level": 4,
                "description": "Distrito"
            }
        }
    ]
}
</pre>
				<br>
				<h3><b>/provinces/{province_id}/districts/{district_id}/posts/{post_id}</b></h3>

				<p><b>URL complecta: </b><code>http://infoice.herokuapp.com/api/v1.0/provinces/{province_id}/districts/{district_id}/posts/post_id?key=YOUR_APP_KEY</code></p>
				<p><b>Descrição: </b>Retorna um Posto Administrativo por ID, que pertencem à um Districto definido por <code>district_id</code>.</p>

				<p><b>Exemplo: </b><code>http://infoice.herokuapp.com/api/v1.0/provinces/4/districts/42/posts/48?key=YOUR_APP_KEY</code></p>
				<br>
<pre>
{
    "place": {
        "id": 48,
        "name": "Calanga",
        "level": 5,
        "description": "Posto Administrativo",
        "in_place": {
            "id": 42,
            "name": "Manhiça",
            "level": 4,
            "description": "Distrito"
        }
    }
}
</pre>



<br>
				<h3 id="places"><b>/places/{id}</b></h3>
					<p><b>URL complecta: </b><code>http://infoice.herokuapp.com/api/v1.0/places/{id}?key=YOUR_APP_KEY</code></p>
					<p><b>Descrição: </b> Permite retornar um valor(<code>place</code>) especifico de acordo com o id.</p>

					<p>Exemplo:</p>
					<code>http://infoice.herokuapp.com/api/v1.0/places/29?key=YOUR_APP_KEY</code>
					<p><b>Resposta</b></p>
<pre>
{
    "place": {
        "id": 29,
        "name": "Cahora-Bassa",
        "level": 4,
        "description": "Distrito",
        "in_place": {
            "id": 10,
            "name": "Tete",
            "level": 3,
            "description": "Província"
        }
    }
}
</pre>




					<br>
					<h3><b>/places?parameters</b></h3>
					<p><b>URL complecta: </b><code>http://infoice.herokuapp.com/api/v1.0/places?parameters&key=YOUR_APP_KEY</code></p>

					<p><b>Descrição: </b> Realiza uma pesquisa de places de acordo com parametros pre-definidos.</p>
				
					<p><b>Parâmetros: </b></p>
					<ul>
						<li><p><code>name:</code>Variável permite definir nome de places que seram pesquisados. Por exemplo, se definir-se <code>name=maput</code> seram retornados places que tenham um name similar.</p></li>
						<li><p><code>level:</code> Variável permite definir o nível dos places que se pretende pesquisar. Por exemplo, quando pretende-se postos administrativos <code>level=5 .</code></p></li>
					</ul>
					
					<br>
					<p>Exemplo:</p>
					<p><code>http://infoice.herokuapp.com/api/v1.0/places?name=Nam&key=YOUR_APP_KEY</code></p>
					
					<p>Esta requisição faz uma busca de todos os <code>places</code> que contem a expressão "Nam" definido em <code>name</code>.</p>
					<p>Resposta:</p>
<pre>
{
    "places": [
        {
            "id": 11,
            "name": "Nampula",
            "level": 3,
            "description": "Província",
            "in_place": {
                "id": 2,
                "name": "Moçambique",
                "level": 2,
                "description": "País"
            }
        },
        {
            "id": 35,
            "name": "Namarroi",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 9,
                "name": "Zambézia",
                "level": 3,
                "description": "Província"
            }
        },
        {
            "id": 47,
            "name": "Namaacha",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 4,
                "name": "Maputo Província",
                "level": 3,
                "description": "Província"
            }
        }
    ]
}	
</pre>

				<br><br>
				<p>Exemplo:</p>
				<p><code>http://infoice.herokuapp.com/api/v1.0/places?name=Nam&level=4&key=YOUR_APP_KEY</code></p>
				
				<p>Esta requisição faz uma busca de todos os <code>places</code> que contem a expressão "Nam" definido em <code>name</code> e seja de <code>level</code> 4.</p>

<pre>
{
    "places": [
        {
            "id": 35,
            "name": "Namarroi",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 9,
                "name": "Zambézia",
                "level": 3,
                "description": "Província"
            }
        },
        {
            "id": 47,
            "name": "Namaacha",
            "level": 4,
            "description": "Distrito",
            "in_place": {
                "id": 4,
                "name": "Maputo Província",
                "level": 3,
                "description": "Província"
            }
        }
    ]
}	
</pre>



				<br><br>
				<!--Inicio da Sessao de ERROR-->
				<h3 id="error">Error</h3>

				<h4>401 status</h4>

<pre><code>
{
    "error": {
        "isKey": false,
        "status": 401,
        "message": "invalid key",
        "more info": "http://infoice.herokuapp.com"
    }
}
</code></pre>


				<h4>404 status</h4>

<pre><code>
{
    "error": {
        "isKey": false,
        "status": 404,
        "message": "Not Found",
        "more info": "http://infoice.herokuapp.com"
    }
}
</code></pre>
			<hr>
		</div>
	</div>
@endsection