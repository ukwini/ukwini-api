<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DistrictController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$name  	  = $request->get('name');
		$key  	  = $request->get('key');

		if(strlen($name) > 0){
			$level = Service::LEVEL_DISTRICT;
			return Service::placeByNameLevel($name, $level, $key);
		}else{
			return Service::placeAllByLevel(Service::LEVEL_DISTRICT, $key);	
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
		$key  	  = $request->get('key');
		$level = Service::LEVEL_DISTRICT;
		return Service::placeById($id, $level, $key);
	}
}
