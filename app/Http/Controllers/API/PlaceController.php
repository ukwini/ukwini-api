<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthController;

use Illuminate\Http\Request;
use App\Place;

use Hash;
use App\Credential;

class PlaceController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$key  	  = $request->get('key');
		$name     = $request->get('name');
		$level    = $request->get('level');

		$places = array();
		$isKey = Service::isKey($key);
		if($isKey['isKey'] == true){

			if(strlen($name) > 0 && strlen($level) > 0){
				if($isKey['isKey'] == true){
					$places = Place::where('name','LIKE','%'.$name.'%')->where('level','=',$level)->get();
					return  Service::responsePlace($places);
				}
			}elseif (strlen($name) > 0 && strlen($level) == 0) {
				$places = Place::where('name','LIKE','%'.$name.'%')->get();
				return  Service::responsePlace($places);

			}elseif (strlen($name) == 0 && strlen($level) > 0) {
				$places = Place::where('level','=',$level)->get();
				return  Service::responsePlace($places);
			}

		}
		
		return Service::placeAllByLevel(100, $key);//Retorn Erro porque nao existe Level 100 (Estava cansado)
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id,Request $request)
	{
		$key = $request->get('key'); //Pega a key da url
		$isKey = Service::isKey($key);//Verifica validade
		$place = new Place;

		if ($isKey['isKey'] == true) {
			$place = Place::where('id','=',$id)->first();
			return Service::responsePlace($place);

		}else{
			return response($isKey,$isKey['status']);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}