<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['prefix'=>'api'],function()
{
	Route::group(['prefix'=>'v1.0'], function(){
		/**
		* Place por ID e 
		* Search por variaveis
		*/
		Route::resource('places','API\PlaceController',['only'=>['show','index']]);

		Route::resource('provinces','API\ProvinceController',['only'=>['show','index']]);

		Route::resource('districts','API\DistrictController',['only'=>['show','index']]);

		//Route::resource('cities','CityController',['only'=>['show','index']]);

		
		Route::resource('provinces.districts','API\ProvinceDistrictController',['only'=>['show','index']]);

		//Route::resource('provinces.cities','API\ProvinceCityController',['only'=>['show','index']]);

		Route::resource('provinces.districts.posts','API\ProvinceDistrictPostController',['only'=>['show','index']]);
	});
});



//Pagina de documentacao
Route::group(['prefix'=>'docs'], function(){
	Route::get('v1.0','DOCS\DocumentationController@index');
});

Route::group(['prefix'=>'admin'], function ()
{
	Route::get('/','AdminController@index');
});

Route::resource('app','AppController',['only'=>['store','destroy']]);