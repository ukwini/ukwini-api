<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Credential;
use App\Place;

class Service extends Controller {

	const LEVEL_COUNTRY  = 2;

	const LEVEL_PROVINCE = 3;

	const LEVEL_DISTRICT = 4;

	const LEVEL_POST  	 = 5;

	const LEVEL_LOCALITY = 6;

	const LEVEL_CITY 	 = 7;

	/**
	*	Verifica a validade uma key e retorna um array 
	* 	Com a situação.
	*/
	public static function isKey($key)
	{
		$result = array('isKey'=>false,
						'status'=>401,
						'message'=>'invalid key',
						'more info'=>'http://infoice.herokuapp.com');

		if (strlen($key) > 10) {// Veifica tamanho do key
			$credential = Credential::where('key','=',$key)->first();
			
			if(count($credential) == 1){//Verifica se key existe, o resultado deve ser 1
				
				if($credential->isActive == 1){//Verifica de o key está activo
					$result['isKey'] = true;
					$result['status'] = 200;
					$result['message'] = 'valid key';
				}

			}
		}

		return $result;
	}

	/**
	* Prepara resposta para uma requisicao de um unico objecto
	*	return: response
	*/
	public static function responsePlace($places)
	{
		$result = array('isKey'=>false,
						'status'=>404,
						'message'=>'Not Found',
						'more info'=>'http://infoice.herokuapp.com');
		if (count($places) > 1){
			//$places = ['places'=>$place];
			$newPlaces = array();

			foreach ($places as $place) {
				$place['description'] = Service::getDescriptionPlace($place->level);//Define Description para place(place)

				$in_place = $place->place;
				unset($in_place['place_id']);//Remove o atributo place_id()

				$in_place['description'] = Service::getDescriptionPlace($in_place->level);//Define Description para Sub_place(in_place)
				$place['in_place'] = $in_place;

				unset($place['place_id']);
				unset($place['place']);
				$newPlaces[] = $place; //Adiciona o place nativo. ou place de origem
			}

			return response()->json(['places'=>$newPlaces],200);


		}elseif (count($places) == 1) {
			//$places = ['places'=>$place];
			$newPlaces = array();

			//foreach ($places as $place) {
				$place = $places;
				$place['description'] = Service::getDescriptionPlace($place->level);//Define Description para place(place)

				$in_place = $place->place;
				unset($in_place['place_id']);//Remove o atributo place_id()

				$in_place['description'] = Service::getDescriptionPlace($in_place->level);//Define Description para Sub_place(in_place)
				$place['in_place'] = $in_place;

				unset($place['place_id']);
				unset($place['place']);
				$newPlaces[] = $place; //Adiciona o place nativo. ou place de origem
			//}
			return response()->json(['place'=>$newPlaces],200);
		}else{ 
			return response(['error'=>$result],404);
		}
		
	}


	/**
	* Retorna todos os places pertecentes a um place_id, de level inferior
	*	
	*/
	public static function placeAllById($id, $level, $key)
	{
		$isKey = Service::isKey($key);//Verifica validade
		$place = array();

		if ($isKey['isKey'] == true) {
			$place = Place::where('place_id','=',$id)->where('level','=',$level)->get();
			return Service::responsePlace($place);

		}else{
			return response(['error'=>$isKey],$isKey['status']);
		}
	}


	/**
	* Retorna todos os places pertecentes a um place_id, de level inferior e que 
	*	corresponde ao ao "name"
	*/
	public static function placeAllByIdName($id, $name, $level, $key)
	{
		$isKey = Service::isKey($key);//Verifica validade
		$place = array();

		if ($isKey['isKey'] == true) {
			$place = Place::where('place_id','=',$id)
								->where('level','=',$level)
								->where('name','LIKE','%'.$name.'%')->get();
			return Service::responsePlace($place);

		}else{
			return response(['error'=>$isKey],$isKey['status']);
		}
	}


	/**
	* Retorna um place por id
	*	
	*/
	public static function placeById($id, $level, $key)
	{
		$isKey = Service::isKey($key);//Verifica validade
		$place = array();

		if ($isKey['isKey'] == true) {
			$place = Place::where('id','=',$id)->where('level','=',$level)->first();
			return Service::responsePlace($place);

		}else{
			return response(['error'=>$isKey],$isKey['status']);
		}
	}


	/**
	* Retorna todos os places pertecentes a um level
	*	
	*/
	public static function placeAllByLevel($level, $key)
	{
		$isKey = Service::isKey($key);//Verifica validade
		$place = array();

		if ($isKey['isKey'] == true) {
			$place = Place::where('level','=',$level)->get();
			return Service::responsePlace($place);

		}else{
			return response(['error'=>$isKey],$isKey['status']);
		}
	}


	/**
	* Retorna um place por id
	*	
	*/
	public static function placeByNameLevel($name, $level, $key)
	{
		$isKey = Service::isKey($key);//Verifica validade
		$place = array();

		if ($isKey['isKey'] == true) {
			$place = Place::where('level','=',$level)->where('name','LIKE','%'.$name.'%')->get();
			return Service::responsePlace($place);

		}else{
			return response(['error'=>$isKey],$isKey['status']);
		}
	}



	public static function placeByIdIdPlace($place_id, $id, $level, $key)
	{
		$isKey = Service::isKey($key);//Verifica validade
		$place = array();

		if ($isKey['isKey'] == true) {
			$place = Place::where('id','=',$id)
								->where('level','=',$level)
								->where('place_id','=',$place_id)->first();
			return Service::responsePlace($place);

		}else{
			return response(['error'=>$isKey],$isKey['status']);
		}
	}




	public static function placeAllByIdId($place_id, $id, $level, $key)
	{
		$isKey = Service::isKey($key);//Verifica validade
		$place = array();

		if ($isKey['isKey'] == true) {

			$place = Place::where('id','=',$id)
								->where('level','=',$level-1)
								->where('place_id','=',$place_id)->first();


			if (count($place) >= 1) {
				$place = Service::placeAllById($place->id, $level, $key); //Pega distrito
				return $place;
			}else{
				return responsePlace($place);
			}

		}else{
			return response(['error'=>$isKey],$isKey['status']);
		}
	}


	public static function placeAllByIdIdName($place_id, $id, $name, $level, $key)
	{
		$isKey = Service::isKey($key);//Verifica validade
		$place = array();

		if ($isKey['isKey'] == true) {

			$place = Place::where('id','=',$id)
								->where('level','=',$level-1)
								->where('place_id','=',$place_id)->first();


			if (count($place) >= 1) {
				$place = Service::placeAllByIdName($id, $name, $level, $key); //Pega distrito
				return $place;
			}else{
				return responsePlace($place);
			}

		}else{
			return response(['error'=>$isKey],$isKey['status']);
		}
	}



	public static function placeByIdIdId($place_id, $place_id1, $id, $level, $key)
	{
		$isKey = Service::isKey($key);//Verifica validade
		$place = array();

		if ($isKey['isKey'] == true) {

			$place = Place::where('id','=',$place_id1)
								->where('level','=',$level-1)
								->where('place_id','=',$place_id)->first();


			if (count($place) >= 1) {
				$place = Service::placeByIdIdPlace($place_id1, $id, $level, $key); //Pega distrito
				return $place;
			}else{
				return Service::responsePlace($place);
			}

		}else{
			return response(['error'=>$isKey],$isKey['status']);
		}
	}

	//Retorna descricao de place deacordo com o level
	public static function getDescriptionPlace($level)
		{
			if ($level == 1) {
				return 'Continente';
			}elseif ($level == 2) {
				return 'País';
			}elseif ($level == 3) {
				return 'Província';
			}elseif ($level == 4) {
				return 'Distrito';
			}elseif ($level == 5) {
				return 'Posto Administrativo';
			}elseif ($level == 6) {
				return 'Localidade';
			}elseif ($level == 7) {
				return 'Cidade';
			}
		}	
}