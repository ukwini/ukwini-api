@extends('app')

@section('header-titulo')
	Administração | {infoice}
@endsection

@section('content')

	<script type="">
		$('#myTabs a').click(function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		});
	</script>


<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#users" aria-controls="home" role="tab" data-toggle="tab">Utilizadores</a></li>

  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="users">
    	
    	<table class="table table-hover">
    		<thead>
    			<tr>
    				<th>Nr</th>
    				<th>Nome</th>
    				<th>Email</th>
    				<th>Qtd de Keys</th>
    				<th>Previlegio</th>
    			</tr>
    		</thead>
    		<tbody>
    		@foreach($users as $u)
    			<tr>
    				<td>{{"---"}}</td>
    				<td>{{$u->name}}</td>
    				<td>{{$u->email}}</td>
    				<td>{{sizeof($u->credentials)}}</td>
    				<td>{{$u->admin}}</td>
    			</tr>
    		@endforeach
    		</tbody>
    	</table>
   
    </div>
  </div>

</div>
@endsection