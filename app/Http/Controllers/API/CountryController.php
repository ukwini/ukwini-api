<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Place;
use App\Config;

class CountryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$countries = Place::where('level','=','2')->get();

		return $countries;
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$country = Place::where('level','=','2')
						->where('id','=',$id)->get();

		return $country;
	}


	public function findByVar(Request $request)
	{
		# code...
	}
}