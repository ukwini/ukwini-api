<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ProvinceDistrictController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($provinceId, Request $request)
	{
		$name  	  = $request->get('name');
		$key  	  = $request->get('key');

		$level = Service::LEVEL_DISTRICT;

		if(strlen($name) > 0){
			return Service::placeAllByIdName($provinceId, $name, $level, $key);
		}else{
			return Service::placeAllById($provinceId, $level, $key);	
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($provinceId, $districtId, Request $request)
	{
		$key  	  = $request->get('key');
		$level = Service::LEVEL_DISTRICT;

		return Service::placeByIdIdPlace($provinceId, $districtId, $level, $key);
	}
}