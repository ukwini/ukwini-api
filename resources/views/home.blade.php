
@extends('app')

@section('header-titulo')
	home | {infoice}
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			
			<h3><b> <span class="glyphicon glyphicon-random" aria-hidden="true"></span>&nbsp; Meus credenciais</b></h3>
		
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="col-md-9 col-sm-9">
			<div class="row">
			@foreach($credentials as $credential)
				<div class="col-md-4 col-sm-4">
					<div class="col-md-12 btn btn-default app" data-toggle="collapse" href="#collapseExample{{$i++}}" aria-expanded="false" aria-controls="collapseExample{{$i}}">
						<h4>{{$credential->name_app}}</h4>
					</div>
				</div>
			@endforeach

				<div class="col-md-4 col-sm-4">
					<div class="col-md-12 btn btn-primary app" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
						<h4>Criar Nova Key</h4>
					</div>
				</div>

			</div>
		</div>

		<div class="col-md-3 col-sm-3">
			<div class="collapse" id="collapseExample">
			  <div class="well">
			    	<form action="{{url('/app')}}" method="post" accept-charset="utf-8">
			    		<div class="form-group">
			    			<label for="name_app">Nome da App.</label>
			    			<input class="form-control" type="text" name="name_app" placeholder="Nome da App." required>
			    		</div>

			    		<button type="submit" class="btn btn-default">Criar Key</button>
			    	</form>
			  </div>
			</div>
		</div>

	</div>


	<div class="row keys">
		<div class="col-md-9">
		@foreach($credentials as $credential)
			<div class="collapse" id="collapseExample{{$j++}}">
			  <div class="well">
			  	<div class="row">
			  		<div class="col-md-10">
					    <h3>{{$credential->name_app}}</h3>
					    <h4>Key:&nbsp;&nbsp;{{$credential->key}}</h4>
					    <h4>Criado em:&nbsp;{{$credential->created_at}}</h4>		  			
			  		</div>
			  		<a class="btn btn-danger eliminar_app">Eliminar</a>
			  	</div>


			  </div>
			</div>
		@endforeach
		</div>
	</div>
	
@endsection