@extends('app')

@section('header-titulo')
	{infoice} | API da Divisão administrativa de Moçambique
@endsection


@section('content')

	<div class="row">
		<div class="col-md-12 titulo">
			
			<h2>API da Divisão administrativa de Moçambique</h2>
		
			<hr>
		</div>
	</div>

	<div class="row principal">
		<div class="col-md-6 col-sm-6">
			<img class="img-responsive" src="{{asset('/image/macbook.png')}}" alt="">
		</div>

		<div class="col-md-6 col-sm-6 texto-apresentacao">
			<h4>API simples e organizada com a divisão administrativa de Moçambique</h4>

			<br>
			
			<a href="{{url('/docs/v1.0')}}" class="btn btn-primary btn-lg">Ver Documentação</a>
			<br><br>
			<h4>Ou</h4>
			<br>
			<a href="{{ url('/auth/register') }}" class="btn btn-default btn-lg">Obter Key</a>
		</div>
	</div>	
	<div class="row">
		<div class="col-md-12">
			<hr>
		</div>
	</div>
@endsection